/**
 * @param {string} s
 * @param {string} p
 * @return {number[]}
 */
var findAnagrams = function(s, p) {
  let pSorted = [...p].sort().join('');
  let result = [];
  console.log('#### s = %s, p = %s', s, p)
  for (let i = 0; i < (s.length - p.length + 1); i++) {
      let a = s.substr(i, p.length);
      console.log('#### a = %s, p = %s', a, p)
      if (isAnagram(a, pSorted)) {
          result.push(i);
      }
  }
  return result;
};

let isAnagram = (a, p) => {
  if (a === p) {
      return true;
  }
  return [...a].sort().join('') === p
}

console.log('#### findAnagram = ', findAnagrams("abab", "ab"))
