/**
 * @param {number[][]} points
 * @param {number} K
 * @return {number[][]}
 */
var kClosest = function(points, K) {
  let distances = points.map(point => ({
    point,
    dis: distance(point)
  }));
  let list = distances.sort((a, b) => {
    if (a.dis < b.dis) {
      return -1;
    } else if (a.dis > b.dis) {
      return 1;
    } else {
      return 0;
    }
  });
  return list.slice(0, K).map(el => el.point);
};

const distance = (point = [0, 0]) => {
  return Math.sqrt(point[0] ** 2 + point[1] ** 2);
};
