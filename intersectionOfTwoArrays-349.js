/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
var intersection = function(nums1, nums2) {
  let map1 = {};
  let map2 = {};
  nums1.forEach(el => {
      if (!map1[el]) {
          map1[el] = el;
      }
  });
  nums2.forEach(el => {
      if (!map2[el]) {
          map2[el] = el;
      }
  });
  console.log('#### map1 ', map1)
  console.log('#### map2 ', map2)
  let mapCommon = {};
  for (let key1 in map1) {
    if (!mapCommon[key1]) {
      mapCommon[key1] = 0;
    }
    mapCommon[key1] += 1;
  }
  for (let key2 in map2) {
    if (!mapCommon[key2]) {
      mapCommon[key2] = 0;
    }
    mapCommon[key2] += 1;
  }

  return Object.keys(mapCommon).filter(el => mapCommon[el] > 1).map(el => el*1);
};

console.log('#### intersection = ', intersection([1,2,2,1], [2,2]))
console.log('#### intersection = ', intersection([4,9,5], [9,4,9,8,4]))
