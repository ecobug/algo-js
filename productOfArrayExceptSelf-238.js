/**
 * @param {number[]} nums
 * @return {number[]}
 */
var productExceptSelf = function(nums) {
  let zeroPositions = [];
  nums.forEach((el, i) => {
    if (el === 0) {
      zeroPositions.push(i)
    }
  });
  let production = zeroPositions.length > 1 ? 0 : nums.reduce((acc, cur) => {
    if (cur === 0) {
      return acc;
    } else {
      return acc * cur;
    }
  }, 1);

  return nums.map((num, i) => {
      if (zeroPositions.indexOf(i) === -1) {
        if (zeroPositions.length > 0) {
          return 0;
        } else {
          return production / num;
        }
      } else {
        return production;
      }
  });
};





const input = [1,2,3,4, 0];
console.log('#### productExceptSelf  in', input)
console.log('#### productExceptSelf out', productExceptSelf(input))
