const sorted = (word1, word2, order) => {
  if (word1.startsWith(word2)) {
    return false;
  }
  for (let j = 0; j < Math.min(word1.length, word2.length); j++) {
    // check two words if ordered correctly
    // console.log('#### word1[j], word2[j]', word1[j], word2[j], order.indexOf(word1[j]), order.indexOf(word2[j]));
    if (order.indexOf(word1[j]) < order.indexOf(word2[j])) {
      return true;
    } else if (order.indexOf(word1[j]) === order.indexOf(word2[j])) {
      continue;
    } else {
      return false;
    }
  }
  return true;
};

var isAlienSorted = function(words, order) {
  // iterate words.length -1
  for (let i = 0; i < words.length - 1; i++) {
    // check every char based on min(wordsA, wordsB)
    if (sorted(words[i], words[i + 1], order)) {
      continue;
    } else {
      return false;
    }
  }
  return true;
};

/**
 https://leetcode.com/problems/verifying-an-alien-dictionary/

 */

console.log(
  "#### isAlienSorted = ",
  isAlienSorted(["hello", "leetcode"], "hlabcdefgijkmnopqrstuvwxyz")
);

console.log(
  "#### isAlienSorted = ",
  isAlienSorted(["word", "world", "row"], "worldabcefghijkmnpqstuvxyz")
);

console.log(
  "#### isAlienSorted = ",
  isAlienSorted(["apple", "app"], "abcdefghijklmnopqrstuvwxyz")
);
