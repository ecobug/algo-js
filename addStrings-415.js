/**
 * @param {string} num1
 * @param {string} num2
 * @return {string}
 */
var addStrings = function(num1, num2) {
  let N1 = [...num1];
  let N2 = [...num2];
  let result = [];
  let carry = 0;
  while(N1.length || N2.length) {
      let n1 = N1.length ? N1.pop()*1 : 0;
      let n2 = N2.length ? N2.pop()*1 : 0;
      let sum = n1 + n2 + carry;
      result.unshift(sum % 10);
      carry = Math.floor(sum / 10)
  }
  if (carry > 0) {
    result.unshift(carry);
  }
  return result.join('');
};

console.log('#### addStrings = ', addStrings('213', '100'))
