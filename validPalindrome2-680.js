/**
 * @param {string} s
 * @return {boolean}
 */
/**
 * @param {string} s
 * @return {boolean}
 */

const validPalindrome = (s) => {
  if (isPalindrome(s)) {
      return true;
  }
  for (let i = 0; i < s.length / 2; i++) {
      let j = s.length - i - 1
      if (s[i] !== s[j]) {
          return isPalindrome(omit(s, i)) || isPalindrome(omit(s, j));
      }
  }
  return true
};

const omit = (s, i) => s.substr(0, i) + s.substr(i + 1);
const isPalindrome = (s) => {
for (let i = 0; i < Math.floor(s.length/2); i++) {
    if (s[i] !== s[s.length - i - 1]) {
        return false;
    }
}
return true;
}

console.log('#### validPalindrome("abca") ', validPalindrome("abca"))
console.log('#### validPalindrome("abc") ', validPalindrome("abc"))
console.log('#### validPalindrome("deeee") ', validPalindrome("deeee"))
