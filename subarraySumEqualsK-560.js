/**
 * https://leetcode.com/problems/subarray-sum-equals-k/
 */

var subarraySum = function(nums, k) {
  // nums = [1,2,3]
  let count = 0;
  // populate every sub array i: start index, j : end index
  for (let i = 0; i < nums.length; i++) {
    let sum = 0;
    for (let j = i; j < nums.length; j++) {
      sum += nums[j] || 0;
      if (k === sum) {
        count++;
      }
    }
  }
  return count;
};

// subarraySum([1, 2, 3, 4, 5, 6, 7], 7);
subarraySum([1, 1, 1], 2);
