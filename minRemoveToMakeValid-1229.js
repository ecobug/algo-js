/**
 * https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/submissions/
 */
// "lee(t(c)o)de)"
var minRemoveToMakeValid = function(s) {
  let str = [...s];
  let stack = [];
  for (let i = 0; i < str.length; i++) {
    let value = str[i];
    if (value === "(") {
      stack.push(i);
    } else if (value === ")") {
      if (stack.length > 0) {
        stack.pop();
      } else {
        str[i] = "";
      }
    }
  }
  let result = str
    .map((el, idx) => (stack.indexOf(idx) > -1 ? "" : el))
    .join("");
  console.log("#### str = ", str.join(""), result);
  return result;
};

console.log(
  "#### minRemoveToMakeValid = ",
  minRemoveToMakeValid("lee(t(c)o)de)") === "lee(t(c)o)de"
);
